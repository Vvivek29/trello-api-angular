import { Component, OnInit, Input } from '@angular/core';
import {ChecklistService} from '../services/checklist.service';
import {CardService} from '../services/card.service';

@Component({
  selector: 'app-checklist',
  templateUrl: './checklist.component.html',
  styleUrls: ['./checklist.component.css']
})
export class ChecklistComponent implements OnInit {

  public id:string;
  public input : boolean;
  public cardNamePopUp : boolean;
  public inputCheckList :boolean;
  constructor(private ChecklistService : ChecklistService,
    private CardService: CardService) { }
  @Input() public CheckList:any;
  @Input() public cardId:string;
  @Input() public cardName:string;
  @Input() public UpdateCard:any;
  
  ngOnInit() {

  }
  

  showCheckItem(event){
    this.id=event.target.parentNode.id;
    this.input=!this.input;
  }
  showCheckList(){
    
    this.inputCheckList=!this.inputCheckList;
  }
  addCheckList(event){
  
  this.ChecklistService.addCheckList(event.target.parentNode.id,event.target.parentNode.children[0].value).subscribe(data=>this.CheckList=this.CheckList.concat(data))
  }
  deleteCheckList(event){
    console.log(event.target.parentNode.parentNode.id)
    this.ChecklistService.delete(event.target.parentNode.parentNode.id).subscribe()
   this.CheckList=this.CheckList.filter(item=>item.id!==event.target.parentNode.parentNode.id)
  }
  deleteCheckItem(event){
    this.ChecklistService.deleteCheckItem(event.target.parentNode.parentNode.parentNode.id,event.target.parentNode.children[0].id).subscribe();
  this.CheckList.forEach(item=>{
      if(item.id===event.target.parentNode.parentNode.parentNode.id){
      item.checkItems=item.checkItems.filter(elem=>elem.id!==event.target.parentNode.children[0].id)
      }
    })
  }

  addCheckItem(event){
    this.ChecklistService.addCheckItems(event.target.parentNode.parentNode.id,event.target.parentNode.children[0].value).subscribe(data=>this.CheckList.forEach(item=>{
      if(item.id===event.target.parentNode.parentNode.id){
        item.checkItems=item.checkItems.concat(data)
      }}))
  }

  checkBoxFunction(event){
    this.CheckList.forEach(item=>{
      if(item.id===event.target.parentNode.parentNode.parentNode.id){
        item.checkItems.forEach(elem=>{
          
          if(elem.id===event.target.parentNode.parentNode.children[1].children[0].id){
            if(elem.state=='incomplete'){
              elem.state='complete'
            }else{
              elem.state='incomplete';
            }
            this.ChecklistService.updateCheckBox(this.cardId,event.target.parentNode.parentNode.children[1].children[0].id,elem.state).subscribe(); 
          }})
      }
    })
  }

  updateCardName(){
    this.cardNamePopUp=!this.cardNamePopUp;
  }
addUpdatedName(event){
  this.UpdateCard(this.cardId,event.target.parentNode.children[0].value)
  this.cardName=event.target.parentNode.children[0].value;
}

}
