import { Component, OnInit, Input } from '@angular/core';
import {Card} from '../cards';
import {CardService} from '../services/card.service';
import {CheckList} from '../CheckList';
import {ChecklistService} from '../services/checklist.service';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {

  public updateName:string;
  public updateIdList : string;
  public cardName : string;
  public cardId : string;
  public popUp : boolean;
  public status : boolean;
  public CardData : Card[];
  public CheckList : CheckList[];
  constructor(private CardService : CardService,
    private ChecklistService : ChecklistService) { }
  @Input() public listId :string;
  @Input() public boardData:any;
  ngOnInit() {
    this.CardService.getCard(this.listId).subscribe(card=>{
      this.CardData=card;
      // console.log(card)
    })
  }
  add(listId,name){
    this.CardService.addCard(listId,name).subscribe(card=>this.CardData=this.CardData.concat(card))
  }

  inputBoxOpen(){
    this.status=!this.status;
  }
  inputBoxClose(){
    this.status=!this.status;
  }
  showPopUp(){
   
    this.popUp=!this.popUp;
  }
  getCardId(event){
  this.cardId=event.id;
  this.cardName=event.innerText;
  this.ChecklistService.getCheckList(this.cardId).subscribe(data=>{this.CheckList=data;
  // console.log(this.CheckList)
}
  )

  }
  
  UpdateCard(cardId,name){
     console.log(cardId,name)
    this.updateName=name;
  this.CardService.updateCard(this.cardId,this.updateName).subscribe(data=>
    // test
    // console.log(this.CardData)
    this.updateIdList= data.idList
  )}
}
