import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {Card} from '../cards';


@Injectable({
  providedIn: 'root'
})
export class CardService {

  constructor(private http: HttpClient) { }

  getCard(cardId): Observable<Card[]>{
    
    return this.http.get<Card[]>(`https://api.trello.com/1/list/${cardId}/cards?key=d31dd0ffaf19a6a77a84841bf07bae88&token=c30cf4390a00af4ebcce77fea8633ec39f1e1a96e5cb9fb508fa2dac75d4a82f`);
  }
addCard(listId,cardName):Observable<Card>{
  return this.http.post<Card>(`https://api.trello.com/1/cards?name=${cardName}&pos=bottom&idList=${listId}&keepFromSource=all&key=d31dd0ffaf19a6a77a84841bf07bae88&token=c30cf4390a00af4ebcce77fea8633ec39f1e1a96e5cb9fb508fa2dac75d4a82f`,listId)
}
updateCard(cardId:string,name:string): Observable<any>{
  return this.http.put(`https://api.trello.com/1/cards/${cardId}?name=${name}&key=d31dd0ffaf19a6a77a84841bf07bae88&token=c30cf4390a00af4ebcce77fea8633ec39f1e1a96e5cb9fb508fa2dac75d4a82f`,name)
}
}
