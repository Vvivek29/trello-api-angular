import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {List} from '../list-name';
import {Observable} from 'rxjs';
import {Card} from '../cards';

@Injectable({
  providedIn: 'root'
})
export class BoardServiceService {

  private _url: string = "https://api.trello.com/1/boards/WWudnRON/lists?cards=none&card_fields=all&filter=open&fields=all&key=d31dd0ffaf19a6a77a84841bf07bae88&token=c30cf4390a00af4ebcce77fea8633ec39f1e1a96e5cb9fb508fa2dac75d4a82f"
  constructor(private http: HttpClient) { }

  getList(): Observable<List[]>{
  // console.log(this.http.get<List[]>(this._url))

    return this.http.get<List[]>(this._url);
  }


  addList(name): Observable<List>{
    return this.http.post<List>(`https://api.trello.com/1/boards/WWudnRON/lists?name=${name}&pos=top&key=d31dd0ffaf19a6a77a84841bf07bae88&token=c30cf4390a00af4ebcce77fea8633ec39f1e1a96e5cb9fb508fa2dac75d4a82f`,name)

  }
  
}