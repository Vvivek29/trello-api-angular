import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {CheckList} from '../CheckList';
import {CheckItems} from '../checkItems'

@Injectable({
  providedIn: 'root'
})
export class ChecklistService {

public key= 'd31dd0ffaf19a6a77a84841bf07bae88';
public token= 'c30cf4390a00af4ebcce77fea8633ec39f1e1a96e5cb9fb508fa2dac75d4a82f';

  constructor(private http: HttpClient) { }

  getCheckList(cardId:string): Observable<CheckList[]>{
    return this.http.get<CheckList[]>(`https://api.trello.com/1/cards/${cardId}/checklists?key=${this.key}&token=${this.token}`)

  }
  addCheckList(cardId,name): Observable<CheckList>{
    return this.http.post<CheckList>(`https://api.trello.com/1/checklists?idCard=${cardId}&name=${name}&pos=bottom&key=${this.key}&token=${this.token}`,name)
  }
  
  delete(checklistId): Observable<any>{
    return this.http.delete(`https://api.trello.com/1/checklists/${checklistId}?key=${this.key}&token=${this.token}`)
  }

  deleteCheckItem(checklistId,checkItemId): Observable<any>{
    return this.http.delete(`https://api.trello.com/1/checklists/${checklistId}/checkItems/${checkItemId}?key=${this.key}&token=${this.token}`)
  }

  addCheckItems(checklistId,checkItemName): Observable<CheckItems>{
    return this.http.post<CheckItems>(`https://api.trello.com/1/checklists/${checklistId}/checkItems?name=${checkItemName}&key=${this.key}&token=${this.token}`,checkItemName)
  }

  updateCheckBox(cardId,itemId,status): Observable<any>{
    return this.http.put<any>(`https://api.trello.com/1/cards/${cardId}/checkItem/${itemId}?state=${status}&key=${this.key}&token=${this.token}`,status)
  }

  
}
