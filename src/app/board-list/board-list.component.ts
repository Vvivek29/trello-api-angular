import { Component, OnInit } from '@angular/core';
import {BoardServiceService} from '../services/board-service.service';
import {List} from '../list-name';



@Component({
  selector: 'app-board-list',
  templateUrl: './board-list.component.html',
  styleUrls: ['./board-list.component.css']
})
export class BoardListComponent implements OnInit {

  status : boolean=false;
  public BoardData : List[];
  constructor(private BoardService : BoardServiceService) { }
  
  ngOnInit() {
    this.BoardService.getList().subscribe(data=>{
      this.BoardData=data
      console.log(this.BoardData)
        })

        
  }

add(text){
  
  this.BoardService.addList(text).subscribe(data=>this.BoardData=this.BoardData.concat(data))
}
inputBox(){
  this.status=!this.status;
}
inputBoxOpen(){
  this.status=!this.status;
}

}
